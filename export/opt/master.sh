#!/bin/bash

# To run this docker:

# docker run --net host --pid host --privileged -v /:/rootfs:ro -v /etc/kubernetes:/etc/kubernetes -v /sys:/sys:ro -v /dev:/dev -v /var/lib/docker/:/var/lib/docker:ro -v /var/lib/kubelet/:/var/lib/kubelet:rw -v /var/run:/var/run:rw iqhive/k8s /opt/master.sh

exec /hyperkube kubelet --containerized --hostname-override="127.0.0.1" --address="0.0.0.0" --api-servers=http://localhost:8080 --config=/etc/kubernetes/manifests --allow-privileged=true
