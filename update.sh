#/bin/bash

# rsync -a build/kubernetes-1.1.3/_output/dockerized/bin/linux/amd64/* export/usr/local/bin/
rsync -a build/kubernetes-1.1.3/_output/dockerized/bin/linux/amd64/hyperkube export/usr/local/bin/
rsync -a build/kubernetes-1.1.3/_output/dockerized/bin/linux/amd64/kubectl export/usr/local/bin/

rsync -a build/etcd-release-2.2/bin/* export/usr/local/bin/

docker build -t iqhive/k8s .
#./run.sh

