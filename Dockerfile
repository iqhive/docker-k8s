#
# IQ Hive Master Kubernetes Node
#
FROM iqhive/gentoo-patched
#
# FROM iqhive/ubuntu-patched:latest
MAINTAINER Relihan Myburgh <rmyburgh@iqhive.com>

RUN emerge-webrsync; emerge -v net-misc/socat sys-apps/ethtool net-misc/bridge-utils; rm -Rf /usr/portage/*; rm -Rf /var/tmp/portage/*

COPY export/ /

RUN ln -s /usr/local/bin/hyperkube /hyperkube


# RUN apt-get -y install ethtool bridge-utils iptables

